﻿using Listening.Models;
using Listening.Repositories;
using Listening.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Listening.Services.ServiceContracts;
using FluentAssertions;
using Tests;
using Listening.Models.ServiceModels;

namespace ListeningTest.UnitTests
{
    public class ResultServiceSpec
    {
        private ResultService sut;
        private Mock<IResultRepository> _repoMock;
        private Mock<ITextService> _textServiceMock;

        public ResultServiceSpec()
        {
            _repoMock = new Mock<IResultRepository>();
            _textServiceMock = new Mock<ITextService>();
            sut = new ResultService(_repoMock.Object, _textServiceMock.Object);
        }

        class BuildResultStringData : EnumerableDataAbstract
        {
            public BuildResultStringData()
            {
                _data = new List<object[]>
                {
                    new object[] {
                        new string[][] { new string[] { "5", ",", "3", "." } },
                        new int[] { 11, 19 }
                    },
                    new object[] {
                        new string[][] { new string[] { "4", "2", ",", "3", "." } },
                        new int[] { 13, 21 }
                    },
                    new object[] {
                        new string[][] { new string[] { "2", "4", "3", "!!!" } },
                        new int[] { 19 /*, 21, 23*/ }
                    },
                    new object[] {
                        new string[][] {
                            new string[] { "1", "2", "3" },
                            new string[] { "1", "!?", "3" },
                            new string[] { "1", ".", "3" },
                            new string[] { "1", "...", "3" },
                            new string[] { "4", "5", "6" }
                        },
                        new int[] { 15, /*17,*/25, /*27,*/ 35, /*37, 39, 41 */}
                    }
                };
            }
        }

        class MergeEncodingWithTextData : EnumerableDataAbstract
        {
            public MergeEncodingWithTextData()
            {
                _data = new List<object[]> {
                    new object[] {
                        new string[][] { new string[]{ "Nevertheless" } },
                        new byte[] { 0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
                        new string[][][] {
                            new string[][] {
                                new string[] {
                                    "1", "e", "3", "e", "5", "6", "7", "8", "9", "10", "11", "12" }
                            }
                        }
                    },
                    new object[] {
                        new string[][] { new string[]{ "There",
                            "are", "a", "few", "moments", "!?..." } },
                        new byte[] {
                            1,0,1,0,1,0,1,1,1,1,//There: 3 - hinted, 2 - guessed
                            0,0,0,0,0,0,//are: all hidden
                            1,0,//a: hinted
                            1,1,1,1,1,1,//few: all guessed
                            0,0,1,0,0,0,1,0,0,0,1,0,0,0,//moments: hidden-hinted
                            0,1 /*,0,1,0,1,0,1,0,1,*/ //signs
                        },
                        new string[][][] {
                            new string[][] {
                                new string[] { "T","h","e","r","e" },
                                new string[] { "1","2","3" },
                                new string[] { "a" },
                                new string[] { "f","e","w" },
                                new string[] { "1","o","3","e","5","t","7" },
                                new string[] { "!?..." }
                            } }
                    }
                };
            }
        }

        class HintLetterData : EnumerableDataAbstract
        {
            public HintLetterData()
            {
                var wordsInParagraphs1 = new string[][] {
                            new string[] { "Hi", ",", "Black", "!" },
                            new string[] { "Hi", ",", "White", "!",
                                "Welcome", "." }
                        };
                var encodedByteString1 = new byte[] {
                    0,0,0,0, 0,1, 0,0,0,0,0,0,0,0,0,0, 0,1,
                    0,0,0,0, 0,1, 0,0,0,0,0,0,0,0,1,0, 0,1,
                    1,1,1,1,1,1,1,1,1,1,1,1,1,1, 0,1
                };
                var paragrphsSymbolsCounts = new int[] { 9, 9 };

                _data = new List<object[]>
                {
                    new object[] {
                        wordsInParagraphs1,
                        encodedByteString1,
                        paragrphsSymbolsCounts,
                        1,2,3, // check correctness of using paragrphsSymbolsCounts
                        new byte[] { 0,0,0,0, 0,1, 0,0,0,0,0,0,0,0,0,0, 0,1,
                            0,0,0,0, 0,1, 0,0,0,0,0,0,1,0,1,0, 0,1,
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1, 0,1
                        }
                    },
                    new object[] {
                        wordsInParagraphs1,
                        encodedByteString1,
                        paragrphsSymbolsCounts,
                        1,0,1, // check correctness of using paragrphsSymbolsCounts 2
                        new byte[] { 0,0,0,0, 0,1, 0,0,0,0,0,0,0,0,0,0, 0,1,
                            0,0,1,0, 0,1, 0,0,0,0,0,0,0,0,1,0, 0,1,
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1, 0,1
                        }
                    },
                    new object[] {
                        wordsInParagraphs1,
                        encodedByteString1,
                        paragrphsSymbolsCounts,
                        1,1,0, // check possibility to hint sign
                        new byte[] { 0,0,0,0, 0,1, 0,0,0,0,0,0,0,0,0,0, 0,1,
                            0,0,0,0, 0,1, 0,0,0,0,0,0,0,0,1,0, 0,1,
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1, 0,1
                        }
                    },
                    new object[] {
                        wordsInParagraphs1,
                        encodedByteString1,
                        paragrphsSymbolsCounts,
                        1,2,4, // check possibility to hint hinted
                        new byte[] { 0,0,0,0, 0,1, 0,0,0,0,0,0,0,0,0,0, 0,1,
                            0,0,0,0, 0,1, 0,0,0,0,0,0,0,0,1,0, 0,1,
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1, 0,1
                        }
                    },
                    new object[] {
                        wordsInParagraphs1,
                        encodedByteString1,
                        paragrphsSymbolsCounts,
                        1,4,2, // check possibility to hint guessed
                        new byte[] { 0,0,0,0, 0,1, 0,0,0,0,0,0,0,0,0,0, 0,1,
                            0,0,0,0, 0,1, 0,0,0,0,0,0,0,0,1,0, 0,1,
                            1,1,1,1,1,1,1,1,1,1,1,1,1,1, 0,1
                        }
                    },

                    new object[] {
                        new string[][] { new string[]{ "There",
                            "are", "a", "few", "moments", "!?..." } },
                        new byte[] {
                            1,0,1,0,1,0,1,1,1,1,//There: 3 - hinted, 2 - guessed
                            0,0,0,0,0,0,//are: all hidden
                            1,0,//a: hinted
                            1,1,1,1,1,1,//few: all guessed
                            0,0,1,0,0,0,1,0,0,0,1,0,0,0,//moments: hidden-hinted
                            0,1,0,1,0,1,0,1,0,1,//signs
                        },
                        new int[] { 14 },
                        0,1,2,
                        new byte[] {
                            1,0,1,0,1,0,1,1,1,1,//There: 3 - hinted, 2 - guessed
                            0,0,0,0,1,0,//are: one hinted
                            1,0,//a: hinted
                            1,1,1,1,1,1,//few: all guessed
                            0,0,1,0,0,0,1,0,0,0,1,0,0,0,//moments: hidden-hinted
                            0,1,0,1,0,1,0,1,0,1,//signs
                        },
                    }
                };
            }
        }

        [Theory, ClassData(typeof(HintLetterData))]
        public void ShouldHintLetter(string[][] wordsInParagraphs,
            byte[] resultsEncodedByteString, int[] paragrphsSymbolsCounts,
            int paragraphIndex, int wordIndex, int symbolIndex,
            byte[] updateResultsEncodedByteString)
        {
            var resultsEncodedString = resultsEncodedByteString
                .Select(x => Convert.ToBoolean(x)).ToArray();
            var updateResultsEncodedString = updateResultsEncodedByteString
                .Select(x => Convert.ToBoolean(x)).ToArray();
            var existedResult = new Result
            { ResultsEncodedString = resultsEncodedString };

            var newResult = new Result
            {
                UserId = "userId",
                TextId = "textId",
                Mode = 'j'
            };

            var letterAddress = new LetterAddress
            {
                ParagraphIndex = paragraphIndex,
                WordIndex = wordIndex,
                SymbolIndex = symbolIndex
            };

            _repoMock.Setup(x => x.GetNonCompletedResult(It.IsAny<Result>()))
                .ReturnsAsync(existedResult);
            _repoMock.Setup(x => x.Update(It.IsAny<Result>()))
                .Returns(Task.CompletedTask);
            _textServiceMock.Setup(x => x.GetWordsInParagraphs(It.IsAny<string>()))
                .ReturnsAsync(wordsInParagraphs);
            _textServiceMock.Setup(x => x.GetParagrphsSymbolsCounts(It.IsAny<string>()))
                .ReturnsAsync(paragrphsSymbolsCounts);

            sut.HintLetter(newResult, letterAddress)
               .GetAwaiter().GetResult();

            _repoMock.Verify(x => x.Update(It.Is<Result>(
                y => y.ResultsEncodedString.SequenceEqual(updateResultsEncodedString)
                    && y.IsStarted == true)));
        }

        [Fact]
        public void ShouldReturnExisedResult()
        {
            var existedResult = new Result();
            _repoMock.Setup(x => x.GetNonCompletedResult(It.IsAny<Result>()))
                .ReturnsAsync(existedResult);

            var newResult = new Result
            {
                UserId = "userId",
                TextId = "textId",
                Mode = 'j'
            };

            var result = sut.BuildResultString(newResult,
                    (string[][])(new BuildResultStringData().First()[0]))
                .GetAwaiter().GetResult();

            result.Should().Be(existedResult);
        }

        [Theory, ClassData(typeof(MergeEncodingWithTextData))]
        public void ShouldMergeEncodingWithText(string[][] wordsInParagraphs,
            byte[] resultsEncodedByteString, string[][][] resultTextString)
        {
            var textService = new TextService();
            var resultsEncodedString = resultsEncodedByteString
                .Select(x => Convert.ToBoolean(x)).ToArray();

            _textServiceMock.Setup(x => x.IsSpecialSymbolWord(It.IsAny<string>()))
                .Returns(new Func<string, bool>(textService.IsSpecialSymbolWord));
            _textServiceMock.Setup(x => x.GetWordsInParagraphs(It.IsAny<string>()))
                .ReturnsAsync(wordsInParagraphs);

            var result = new Result
            {
                TextId = "texId",
                ResultsEncodedString = resultsEncodedString
            };

            var resultMergedString = sut.MergeEncodingWithText(result)
                .GetAwaiter().GetResult();

            resultMergedString.ShouldBeEquivalentTo(resultTextString);
        }

        [Theory, ClassData(typeof(BuildResultStringData))]
        public void ShouldBuildResultString(string[][] wordsCounts, int[] result)
        {
            var newResult = new Result
            {
                UserId = "userId",
                TextId = "textId",
                Mode = 'j'
            };

            var counts = Count(wordsCounts);
            var expectedEncodedString = new bool[2 * counts];
            foreach (var item in result)
                expectedEncodedString[item] = true;

            _textServiceMock.Setup(x => x.GetSymbolsCount(It.IsAny<string>()))
                .ReturnsAsync(counts);
            _repoMock.Setup(x => x.GetNonCompletedResult(It.IsAny<Result>()))
                .ReturnsAsync((Result)null);
            _repoMock.Setup(x => x.Insert(It.IsAny<Result>()))
                .Returns(Task.CompletedTask);

            sut.BuildResultString(newResult, wordsCounts).GetAwaiter().GetResult();

            _repoMock.Verify(c => c.Insert(It.Is<Result>(t => t.UserId.Equals(newResult.UserId)
                  && t.TextId.Equals(newResult.TextId)
                  && t.ResultsEncodedString.SequenceEqual(expectedEncodedString))));
        }

        private int Count(string[][] wordsCounts)
        {
            int count = 0, x;

            foreach (var paragrapth in wordsCounts)
                foreach (var word in paragrapth)
                    if (int.TryParse(word, out x))
                        count += x;
                    else
                        //count += word.Length;
                        count++;

            return count;
        }
    }
}
