﻿using Listening;
using Listening.Models;
using Listening.Models.AccountViewModels;
using Listening.Repositories;
using Listening.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ListeningTest
{
    public class DatabaseFixture : IDisposable
    {
        private RegisterViewModel _newUser;
        private ResultRepository _resultRepository;
        //private IUserRepository _userRepository;

        public IConfigurationRoot Configuration { get; set; }
        public TestServer Server { get; set; }
        public HttpClient Client { get; set; }
        public ApplicationUser NewUser { get; set; }
        public List<Result> Results { get; set; }

        public DatabaseFixture()
        {
            var currentDir = Path.Combine(Directory.GetCurrentDirectory(), "../Listening");
            var appSettingsPath = Path.Combine(currentDir, "appsettings.json");
            var builder = new ConfigurationBuilder()
                .AddJsonFile(appSettingsPath, optional: false, reloadOnChange: true);
            Configuration = builder.Build();

            var host = new WebHostBuilder()
                .UseConfiguration(Configuration)
                .UseContentRoot(currentDir)
                .UseStartup<Startup>();

            Server = new TestServer(host);
            Client = Server.CreateClient();

            _newUser = new RegisterViewModel
            {
                Email = "dasistfantastisch125@gmail.com",
                Password = "123qweASD!@#",
                ConfirmPassword = "123qweASD!@#"
            };

            CreateNewUser();
            NewUser = GetNewUser();
            GenerateUserResults();
        }

        public List<Result> GenerateResults(int shift = 0, int count = 5)
        {
            var results = new List<Result>();

            for (int i = 0; i < count; i++)
                results.Add(new Result
                {
                    Id = long.MaxValue - i - shift - 1,
                    UserId = NewUser.Id.ToString(),
                    TextId = RandomHelper.String(),
                    Started = new SqlDateTime(DateTime.Now.AddDays(-13)).Value,
                    Finished = new SqlDateTime(DateTime.Now).Value,
                    ResultsEncodedString = RandomHelper.GenerateBoolArray(10),
                    Mode = RandomHelper.RandomCharForMode(),
                    IsStarted = false,
                    IsCompleted = i % 2 == 0
                });

            return results;
        }

        public void Dispose()
        {
            DeleteResults();
            DeleteNewUser();
            Server.Dispose();
            Client.Dispose();
        }

        private void DeleteResults()
        {
            foreach (var result in Results)
                _resultRepository.Delete(result.Id).GetAwaiter().GetResult();
        }

        private ApplicationUser GetNewUser()
        {
            var loginResult = LoginAsAdmin();
            SetAuthCookie(loginResult);

            var jsonInString = JsonConvert.SerializeObject(new string[] { _newUser.Email },
                            new JsonSerializerSettings
                            {
                                ContractResolver = new CamelCasePropertyNamesContractResolver()
                            });

            var userResponse = Client.PostAsync($"/Account/GetUsersByEmails",
                new StringContent(jsonInString, Encoding.UTF8, "application/json")).Result;
            var stream = userResponse.Content.ReadAsStringAsync().Result;
            var user = JsonConvert.DeserializeObject<ApplicationUser[]>(stream).First();
            DropAuthCookie();
            return user;
        }

        private void CreateNewUser()
        {
            var jsonInString = JsonConvert.SerializeObject(_newUser,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
            var response = Client.PostAsync("/Account/Register",
                new StringContent(jsonInString, Encoding.UTF8, "application/json")).Result;
            response.EnsureSuccessStatusCode();
        }

        private void DeleteNewUser()
        {
            var loginResult = LoginAsAdmin();
            SetAuthCookie(loginResult);
            var deleteResult = Client.DeleteAsync($"/Account/Delete?email={_newUser.Email}").Result;
            deleteResult.EnsureSuccessStatusCode();
        }

        private void SetAuthCookie(HttpResponseMessage response)
        {
            var authCookies = response.Headers.GetValues("Set-Cookie").First();
            var indexOfFirstSplitter = authCookies.IndexOf('=');
            var indexOfLastSplitter = authCookies.IndexOf(';');
            var key = authCookies.Substring(0, indexOfFirstSplitter);
            var value = authCookies.Substring(indexOfFirstSplitter + 1, authCookies.Length - indexOfFirstSplitter - (authCookies.Length - indexOfLastSplitter) - 1);
            Client.DefaultRequestHeaders.Add("Cookie", new CookieHeaderValue(key, value).ToString());
        }

        private void DropAuthCookie()
        {
            Client.DefaultRequestHeaders.Remove("Cookie");
        }

        private HttpResponseMessage LoginAsAdmin()
        {
            var admin = SecurityRulesSingleton.Instance.Rules.Users.First(u => u.Role.Equals("admin"));

            var jsonInString = JsonConvert.SerializeObject(
                new LoginViewModel { Email = admin.Email, Password = admin.Password },
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });

            var loginResult = Client.PostAsync("/Account/Login",
                new StringContent(jsonInString, Encoding.UTF8, "application/json")).Result;
            loginResult.EnsureSuccessStatusCode();
            return loginResult;
        }

        private void GenerateUserResults()
        {
            Results = GenerateResults(20);
            _resultRepository = new ResultRepository(Configuration);
            foreach (var result in Results)
                _resultRepository.Insert(result).GetAwaiter().GetResult();
        }
    }
}
