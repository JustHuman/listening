﻿using FluentAssertions;
using Listening.Models;
using Listening.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using System.Linq;
using System.Data.SqlTypes;

namespace ListeningTest.IntegrationTests
{
    public class ResultsRepositorySpec : IClassFixture<DatabaseFixture>
    {
        private DatabaseFixture _fixture;
        private ResultRepository _resultRepository;
        //private List<Result> _results;

        public ResultsRepositorySpec(DatabaseFixture fixture)
        {
            _fixture = fixture;
            _resultRepository = new ResultRepository(_fixture.Configuration);
        }

        [Fact]
        public async Task CheckCRUD()
        {
            var results = _fixture.GenerateResults(30, 3);

            foreach (var result in results)
                await _resultRepository.Insert(result);

            foreach (var result in results)
                (await _resultRepository.GetById(result.Id)).ShouldBeEquivalentTo(result);

            results = _fixture.GenerateResults(30, 3);

            foreach (var result in results)
                await _resultRepository.Update(result);

            foreach (var result in results)
                (await _resultRepository.GetById(result.Id)).ShouldBeEquivalentTo(result);

            foreach (var result in results)
                await _resultRepository.Delete(result.Id);
        }

        [Fact]
        public async Task CheckBuildResult()
        {
            var id = long.MaxValue - 10;
            var result = new Result
            {
                Id = id,
                UserId = _fixture.NewUser.Id.ToString(),
                TextId = RandomHelper.String(),
                Started = new SqlDateTime(DateTime.Now.AddDays(-13)).Value,
                ResultsEncodedString = RandomHelper.GenerateBoolArray(10),
                Mode = RandomHelper.RandomCharForMode(),
                IsStarted = false,
                IsCompleted = false
            };

            await _resultRepository.Insert(result);

            var nonCompletedResult = await _resultRepository.GetNonCompletedResult(
                new Result
                {
                    UserId = result.UserId,
                    TextId = result.TextId,
                    Mode = result.Mode
                });

            nonCompletedResult.ResultsEncodedString.ShouldBeEquivalentTo(result.ResultsEncodedString);
            nonCompletedResult.Finished.Should().BeNull();
            nonCompletedResult.IsCompleted.Should().BeFalse();

            await _resultRepository.Delete(id);
        }

        [Fact]
        public async Task ShouldReturnNullIfNoElementWithSuchId()
        {
            (await _resultRepository.GetById(long.MaxValue - 100))
                .Should().BeNull();
        }

        [Fact]
        public async Task ShouldReturnNullIfNoElementWithIncorrectTextId()
        {
            (await _resultRepository.GetNonCompletedResult(
                new Result
                {
                    UserId = _fixture.NewUser.Id.ToString(),
                    TextId = RandomHelper.String(),
                    Mode = RandomHelper.RandomCharForMode()
                }))
                .Should().BeNull();
        }

        [Fact]
        public async Task ShouldReturnNullIfNoElementWithIncorrectUserId()
        {
            (await _resultRepository.GetNonCompletedResult(
                new Result
                {
                    UserId = Guid.NewGuid().ToString(),
                    TextId = _fixture.Results.First().TextId,
                    Mode = RandomHelper.RandomCharForMode()
                })).Should().BeNull();
        }

        [Fact]
        public async Task ShouldReturnNullIfNoElementWithWrongUserAndTextId()
        {
            (await _resultRepository.GetNonCompletedResult(
                new Result
                {
                    UserId = Guid.NewGuid().ToString(),
                    TextId = RandomHelper.String(),
                    Mode = RandomHelper.RandomCharForMode()
                }))
                .Should().BeNull();
        }

        //private List<Result> GenerateData(int shift = 0)
        //{
        //    var results = new List<Result>();

        //    for (int i = 0; i < 3; i++)
        //        results.Add(new Result
        //        {
        //            Id = long.MaxValue - i - shift - 1,
        //            UserId = _fixture.NewUser.Id.ToString(),
        //            TextId = RandomHelper.String(),
        //            Started = new SqlDateTime(DateTime.Now.AddDays(-13)).Value,
        //            Finished = new SqlDateTime(DateTime.Now).Value,
        //            ResultsEncodedString = RandomHelper.GenerateBoolArray(10),
        //            Mode = RandomHelper.RandomCharForMode(),
        //            IsCompleted = i % 2 == 0
        //        });

        //    return results;
        //}
    }
}
