﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Listening.Exceptions
{
    public class DataException : Exception
    {
        public DataException(string message) : base(message) { }
    }
}
