using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography.X509Certificates;
using Listening.Security;

namespace Listening
{
    public class Program
    {
        // first param is current directory (we should use it in tests), I also return webhost  
        // due to necessarity of closing
        public static void Main(string[] args)
        {
            //var currentDirectory = (args != null && args.Length > 0 && !string.IsNullOrEmpty(args[0]))
            //    ? args[0]
            //    : Directory.GetCurrentDirectory();

            var config = new ConfigurationBuilder()
                .AddCommandLine(args)
                .AddEnvironmentVariables(prefix: "ASPNETCORE_")
                .Build();

            var certName = SecurityRulesSingleton.Instance.Rules.CertificateName;
            var password = SecurityRulesSingleton.Instance.Rules.Passwd;
            var pfxFile = Path.Combine(Directory.GetCurrentDirectory(), certName);
            var certificate = new X509Certificate2(pfxFile, password);

            var host = new WebHostBuilder()
                .UseConfiguration(config)
                // .UseKestrel(opt => opt.UseHttps(certificate))
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();

            //return host;
        }
    }
}
