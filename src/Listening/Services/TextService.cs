﻿using AutoMapper;
using Listening.Exceptions;
using Listening.Models;
using Listening.Models.Text;
using Listening.Models.TextViewModels;
using Listening.Repositories;
using Listening.ServiceModels;
using Listening.Services.ServiceContracts;
using Listening.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Listening.Services
{
    public class TextService : ITextService
    {
        private readonly IRepository<Text, string> _textRepository;
        private string[] _specialSymbols;
        private char[] _specialCharacters;
        private char[] _moneySymbols;
        //private GlobalCache _globalCache;
        private GlobalCache<TextEnhanced, string> _textCache;
        //private GlobalCache<Result, long> _resultsCache;
        private const int MaxLength = 4000;

        public TextService()
        {
            _specialSymbols = new string[] { ",", ".", "?", ":", ";", "-", "!" };
            _specialCharacters = _specialSymbols.Select(char.Parse).ToArray();
            _moneySymbols = new char[] { '$', '£', '€' };
        }

        public TextService(IRepository<Text, string> repository,
            GlobalCache<TextEnhanced, string> textCache,
            GlobalCache<Result, long> resultsCache) : this()
        {
            _textRepository = repository;
            _textCache = textCache;
        }

        public bool IsSpecialSymbolWord(string word)
        {
            return word.All(x => _specialCharacters.Contains(x) || _moneySymbols.Contains(x));
        }

        public void ResaveAndRecalculateAllTexts()
        {
            var texts = _textRepository.GetAll().Result;
            foreach (var text in texts)
            {
                text.SymbolsCount = GetSymbolCount(GetWordCounts(text.WordsInParagraphs));
                _textRepository.Update(text);
            }
        }

        //public async Task<Text> GetText(string textId)
        //{
        //    var cachedTextDto = _textCache.GetCached(textId);
        //    if (cachedTextDto != null)
        //        return Mapper.Map<Text>(cachedTextDto);

        //    return await _textRepository.GetById(textId);
        //}

        // TODO: looks similar with the next one, perhaps should be refactored
        public async Task<string[][]> GetWordCounts(string textId)
        {
            var cachedTextDto = _textCache.GetCached(textId);
            if (cachedTextDto != null)
                return cachedTextDto.CountsInParagraphs;

            if (_textCache.UseCache)
                await ReadAndInsertToCacheText(textId);
            else
                return GetWordCounts((await _textRepository.GetById(textId)).WordsInParagraphs);

            return await GetWordCounts(textId);
        }

        public async Task<string[][]> GetWordsInParagraphs(string textId)
        {
            var cachedTextDto = _textCache.GetCached(textId);
            if (cachedTextDto != null)
                return cachedTextDto.WordsInParagraphs;

            if (_textCache.UseCache)
                await ReadAndInsertToCacheText(textId);
            else
                return (await _textRepository.GetById(textId)).WordsInParagraphs;

            return await GetWordsInParagraphs(textId);
        }

        public async Task<int[]> GetParagrphsSymbolsCounts(string textId)
        {
            var cachedTextDto = _textCache.GetCached(textId);
            if (cachedTextDto != null)
                return cachedTextDto.ParagrphsSymbolsCounts;

            if (_textCache.UseCache)
                await ReadAndInsertToCacheText(textId);
            else
                return GetParagrphsSymbolsCounts(
                    (await _textRepository.GetById(textId))
                        .WordsInParagraphs);

            return await GetParagrphsSymbolsCounts(textId);
        }

        public async Task<int> GetSymbolsCount(string textId)
        {
            var cachedTextDto = _textCache.GetCached(textId);
            if (cachedTextDto != null)
                return cachedTextDto.SymbolsCount;

            if (_textCache.UseCache)
                await ReadAndInsertToCacheText(textId);
            else
                return (await _textRepository.GetById(textId)).SymbolsCount;

            return await GetSymbolsCount(textId);
        }

        public string[][] GetWordCounts(string[][] wordsInParagraphs)
        {
            var wordsCounts = new List<string[]>();

            foreach (var hiddenWordsInParagraphs in wordsInParagraphs)
            {
                var hiddenWordsLengthInText = new List<string>();

                foreach (var word in hiddenWordsInParagraphs)
                    if (IsSpecialSymbolWord(word))
                        hiddenWordsLengthInText.Add(word);
                    else
                        hiddenWordsLengthInText.Add(word.Length.ToString());

                wordsCounts.Add(hiddenWordsLengthInText.ToArray());
            }

            return wordsCounts.ToArray();
        }

        public async Task<TextDto> GetTextDtoById(string textId)
        {
            var cachedTextDto = _textCache.GetCached(textId);
            if (cachedTextDto != null)
                return cachedTextDto;

            return await ReadAndInsertToCacheText(textId);
        }

        public void Insert(TextDto textDto)
        {
            CheckText(textDto);
            Text text = GenerateTextByDto(textDto);
            _textRepository.Insert(text);
            InsertToCache(textDto, text.WordsInParagraphs);
        }

        public void Update(string id, TextDto textDto)
        {
            CheckText(textDto);
            Text text = GenerateTextByDto(textDto);
            _textRepository.Update(text);
            _textCache.Delete(id);
            InsertToCache(textDto, text.WordsInParagraphs);
        }

        public void Delete(string id)
        {
            _textRepository.Delete(id);
            _textCache.Delete(id);
        }

        private async Task<TextDto> ReadAndInsertToCacheText(string textId)
        {
            var text = await _textRepository.GetById(textId);
            var textDto = Mapper.Map<TextDto>(text);
            textDto.Text = GetTextFromArray(text).ToString();
            InsertToCache(textDto, text.WordsInParagraphs);
            return textDto;
        }

        private void InsertToCache(TextDto textDto, string[][] wordsInParagraphs)
        {
            var textCache = Mapper.Map<TextEnhanced>(textDto);
            textCache.WordsInParagraphs = wordsInParagraphs;
            textCache.CountsInParagraphs = GetWordCounts(wordsInParagraphs);
            textCache.ParagrphsSymbolsCounts = GetParagrphsSymbolsCounts(wordsInParagraphs);
            _textCache.Insert(textCache);
        }

        private int[] GetParagrphsSymbolsCounts(string[][] wordsInParagraphs)
        {
            return wordsInParagraphs
                .Select(x => x.Select(y => y.Length).ToArray().Sum())
                .ToArray();
        }

        private StringBuilder GetTextFromArray(Text text)
        {
            var sb = new StringBuilder();
            foreach (var paragraph in text.WordsInParagraphs)
            {
                foreach (var wordOrSymbol in paragraph)
                    if (_specialSymbols.Except(new string[] { "-" }).Contains(wordOrSymbol))
                        sb.Append($"{wordOrSymbol}");
                    else
                        sb.Append($" {wordOrSymbol}");

                sb.AppendLine();
            }

            return sb;
        }

        private Text GenerateTextByDto(TextDto textDto)
        {
            var formattedText = textDto.Text.Replace("’", "'").Replace("  ", " ");
            foreach (var symbol in _specialSymbols)
                formattedText = formattedText.Replace($" {symbol}", $"{symbol}");

            var paragraphs = formattedText
                .Split(new string[] { "\r\n", "\n", "\n " }, StringSplitOptions.RemoveEmptyEntries);
            var wordsInParagraphs = new List<string[]>();
            //var symbolsCount = 0;

            foreach (var paragraph in paragraphs)
            {
                var words = new List<string>();
                foreach (var word in paragraph.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries))
                    DevideWordAndSpecialSymbols(words, word);
                //symbolsCount += words.Sum(x => x.Count());
                wordsInParagraphs.Add(words.ToArray());
            }

            var text = Mapper.Map<Text>(textDto);
            text.WordsInParagraphs = wordsInParagraphs.ToArray();
            //text.SymbolsCount = symbolsCount;

            return text;
        }

        private void DevideWordAndSpecialSymbols(List<string> words, string word)
        {
            var specialSymbols = _specialSymbols.Select(x => Convert.ToChar(x));
            var endIndexer = word.Length;
            var startIndexer = 0;

            if (_moneySymbols.Contains(word.First()))
            {
                words.Add(word.First().ToString());
                startIndexer++;
            }

            while (specialSymbols.Contains(word[endIndexer - 1]))
                endIndexer--;

            if (endIndexer != startIndexer)
                words.Add(word.Substring(startIndexer, endIndexer - startIndexer));

            if (word.Length != endIndexer)
                words.Add(word.Substring(endIndexer, word.Length - endIndexer));
        }

        private void CheckText(TextDto textDto)
        {
            var sb = new StringBuilder();

            if (string.IsNullOrEmpty(textDto.Title))
                sb.AppendLine($"Title shouldn`t be empty");
            if (textDto.Text == null)
                sb.AppendLine($"Text shouldn`t be empty");
            else if (textDto.Text.Length > MaxLength)
                sb.AppendLine($"System does not support texts more than {MaxLength}");

            if (sb.Length > 0)
                throw new TextException(sb.ToString());
        }

        private int GetSymbolCount(string[][] wordsCounts)
        {
            int count = 0, x;

            foreach (var paragrapth in wordsCounts)
                foreach (var word in paragrapth)
                    if (int.TryParse(word, out x))
                        count += x;
                    else
                        //count += word.Length;
                        count++;

            return count;
        }
    }
}
