﻿using System.Threading.Tasks;
using Listening.Models.TextViewModels;

namespace Listening.Services.ServiceContracts
{
    public interface ITextService
    {
        void Delete(string id);
        Task<int> GetSymbolsCount(string textId);
        Task<TextDto> GetTextDtoById(string textId);
        string[][] GetWordCounts(string[][] wordsInParagraphs);
        Task<string[][]> GetWordCounts(string textId);
        Task<string[][]> GetWordsInParagraphs(string textId);
        void Insert(TextDto textDto);
        void ResaveAndRecalculateAllTexts();
        void Update(string id, TextDto textDto);
        bool IsSpecialSymbolWord(string word);
        Task<int[]> GetParagrphsSymbolsCounts(string textId);
    }
}