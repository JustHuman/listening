﻿using Listening.Models;
using Listening.Models.ServiceModels;
using Listening.Repositories;
using Listening.Services.ServiceContracts;
using Listening.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Listening.Services
{
    // Coding symbols system
    // 00 - hidden symbol
    // 01 - sign
    // 10 - hinted
    // 11 - guessed
    // Example (counts): 
    //      Entry string: 1   2     4           1  2     ,  1  3         !?...
    //      Encoded:      00  00|00 00|00|00|00 00 00|00 01 00 00|00|00  01|01|01|01|01
    public class ResultService
    {
        private const string digitPattern = @"^\d+$";
        private readonly IResultRepository _resultRepository;
        private readonly ITextService _textService;

        public ResultService(IResultRepository resultRepository, ITextService textService)
        {
            _resultRepository = resultRepository;
            _textService = textService;
        }

        public async Task<string[][][]> MergeEncodingWithText(Result result)
        {
            var wordsInParagraphs = await _textService.GetWordsInParagraphs(result.TextId);
            var encodedIndex = 0;
            //var sb = new StringBuilder();
            var mergedText = new List<string[][]>();

            //var ii = 0;

            foreach (var paragraph in wordsInParagraphs)
            {
                var newParagraph = new List<string[]>();

                foreach (var word in paragraph)
                {
                    if (_textService.IsSpecialSymbolWord(word))
                    {
                        newParagraph.Add(new string[] { word });
                        encodedIndex += word.Length * 2;
                        continue;
                    }

                    //sb.Clear();
                    //if (word.Length > sb.Length)
                    //    sb.Length = word.Length;

                    //ii++;
                    //if (ii >= 63)
                    //{
                    //    Console.WriteLine();
                    //}

                    var wordMerged = new List<string>();

                    for (int k = 0; k < word.Length; k++, encodedIndex += 2)
                    {
                        //if (k==9)
                        //{

                        //}
                        //Console.WriteLine(word);
                        wordMerged.Add(result.ResultsEncodedString[encodedIndex]
                            ? word[k].ToString() : (k + 1).ToString());
                        //sb[k] = ;
                    }

                    newParagraph.Add(wordMerged.ToArray());
                    //newParagraph.Add(sb.ToString());
                }

                mergedText.Add(newParagraph.ToArray());
            }

            return mergedText.ToArray();
        }

        public async Task HintLetter(Result result, LetterAddress letterAddress)
        {
            var existedResult = await _resultRepository.GetNonCompletedResult(result);
            var wordsInParagraphs = await _textService.GetWordsInParagraphs(result.TextId);
            var paragrphsSymbolsCounts = await _textService.GetParagrphsSymbolsCounts(result.TextId);

            var paragraphIndex = paragrphsSymbolsCounts.Take(letterAddress.ParagraphIndex).Sum();
            var wordIndex = wordsInParagraphs[letterAddress.ParagraphIndex]
                .Take(letterAddress.WordIndex).Select(x => x.Length).Sum();
            var resultsIndex = paragraphIndex + wordIndex + letterAddress.SymbolIndex;

            if (!existedResult.ResultsEncodedString[resultsIndex * 2]
                && !existedResult.ResultsEncodedString[resultsIndex * 2 + 1])
                existedResult.ResultsEncodedString[resultsIndex * 2] = true;

            existedResult.IsStarted = true;
            await _resultRepository.Update(existedResult);
        }

        public async Task<Result> BuildResultString(Result result, string[][] wordsCounts/*, int symbolsCount*/)
        {
            var existedResult = await _resultRepository.GetNonCompletedResult(result);

            if (existedResult != null)
                return existedResult;

            var symbolsCount = await _textService.GetSymbolsCount(result.TextId);
            var resultEncodedString = BuildStartedResultString(symbolsCount, wordsCounts);

            result.ResultsEncodedString = resultEncodedString;
            result.Started = DateTime.Now;
            await _resultRepository.Insert(result);

            return result;
        }

        private bool[] BuildStartedResultString(int symbolsCount, string[][] wordsCounts)
        {
            var resultEncodedString = new bool[symbolsCount * 2];
            int currentPosition = 0;
            int symbolsCountInWord;

            // run through all paragraphs and mark all signed symbols 
            // (we needn't mark because it`s already marked)
            foreach (var paragraph in wordsCounts)
                foreach (var item in paragraph)
                    if (int.TryParse(item, out symbolsCountInWord))
                        currentPosition += symbolsCountInWord * 2;
                    else
                    //for (int i = 0; i < item.Length * 2; i += 2, currentPosition += 2)
                    {
                        resultEncodedString[currentPosition + 1] = true;
                        currentPosition += 2;
                    }
            return resultEncodedString;
        }
    }
}
