﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Net.Http.Headers;
using System.IO;
using Listening.Exceptions;
using System.Net;
using Microsoft.AspNetCore.Http;
using Listening.Extensions;

namespace Listening.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    public class FileController : Controller
    {
        private string _path;

        public FileController(IConfigurationRoot configuration)
        {
            _path = Directory.GetCurrentDirectory() + configuration["FileStorage:AudioPath"];
        }

        [HttpPut("{id}/{fileName}")]
        public JsonResult PutAudioFile(string id, string fileName, IList<IFormFile> files)
        {
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    if (string.IsNullOrEmpty(fileName))
                        fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName
                            .Trim('"').Replace("\\", "").Replace("/", "");

                    var filesInFolder = Directory.GetFiles(_path);
                    filesInFolder = filesInFolder
                        .Select(x => x.Split('/').Last()).ToArray();

                    if (filesInFolder.Contains(fileName))
                        //throw new FileUploadException($"File with name {fileName} is already exists.");
                        fileName = GenerateUniqueFileName(filesInFolder, fileName);

                    using (var fs = new FileStream(Path.Combine(_path, fileName), FileMode.Create))
                    {
                        file.CopyTo(fs);
                    }
                }
            }

            Response.StatusCode = (int)HttpStatusCode.OK;
            return Json(fileName);
        }

        [HttpDelete("{name}")]
        public JsonResult DeleteFile(string name)
        {
            var fullPath = $"{_path}/{name}";

            if (System.IO.File.Exists(fullPath))
                System.IO.File.Delete(fullPath);
            else
                throw new FileUploadException($"Can`t remove file with name {name} due to his inexistence.");

            Response.StatusCode = (int)HttpStatusCode.OK;
            return Json("");
        }

        private string GenerateUniqueFileName(string[] filesInFolder, string fileName)
        {
            var splitter = fileName.LastIndexOf('.');
            var type = fileName.Substring(splitter, fileName.Length - splitter);
            var name = fileName.Substring(0, splitter);
            string newFileName;
            var index = 0;
            var additionalSymbolsCount = 1;

            // this magic checks possible file name, tries 5 times to generate random symbols
            // and increase random symbols count if no luck
            do
            {
                newFileName = $"{name}{"".GenerateRandomSymbols(additionalSymbolsCount)}{type}";
                index++;
                if (index % 5 == 0)
                    additionalSymbolsCount++;
            }
            while (filesInFolder.Contains(newFileName));

            return newFileName;
        }
    }
}
