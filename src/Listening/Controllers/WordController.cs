﻿using AutoMapper;
using Listening.Models;
using Listening.Models.ServiceModels;
using Listening.Models.Text;
using Listening.Models.TextViewModels;
using Listening.Repositories;
using Listening.Services;
using Listening.Services.ServiceContracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Listening.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class WordController : BaseController
    {
        private readonly ITextService _textService;
        private readonly ResultService _resultService;

        public WordController(ITextService textService, ResultService resultService)
        {
            _textService = textService;
            _resultService = resultService;
        }

        [HttpGet("wordsInParagraphs/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<TextDto> GetWordsInParagraphs(string id)
        {
            return await _textService.GetTextDtoById(id);
        }

        [AllowAnonymous]
        //[Authorize(Roles = "Admin,User")]
        [HttpGet("{id}/{mode}")]
        public async Task<JsonResult> GetWordsCountInParagraphs(string id, char mode)
        {
            var wordsCounts = await _textService.GetWordCounts(id);

            //try
            //{
            //    var user = (await GetCurrentUserAsync()).Id;
            //}
            //catch (Exception ex)
            //{

            //    throw;
            //}

            // TODO: uncomment all this
            //var result = new Result
            //{
            //    UserId = (await GetCurrentUserAsync()).Id,
            //    TextId = id,
            //    Mode = mode
            //};

            //var newResult = await _resultService.BuildResultString(result, wordsCounts);

            Response.StatusCode = (int)HttpStatusCode.OK;

            //if (!newResult.IsStarted)
                return Json(new { wordsCounts, IsStarted = false });

            //var mergedText = await _resultService.MergeEncodingWithText(newResult);

            //return Json(new
            //{
            //    IsStarted = true,
            //    mergedText,
            //    Encoded = newResult.ResultsEncodedString
            //});
        }

        [AllowAnonymous]
        //[Authorize(Roles = "Admin,User")]
        [HttpGet("letter/{id}/{paragraphIndex}/{wordIndex}/{symbolIndex}/{mode}")]
        public async Task<JsonResult> GetLetter(string id,
            int paragraphIndex, int wordIndex, int symbolIndex, char mode)
        {
            var wordsInParagraphs = await _textService.GetWordsInParagraphs(id);

            // TODO: uncomment when it will be completed
            //var result = new Result
            //{
            //    UserId = (await GetCurrentUserAsync()).Id,
            //    TextId = id,
            //    Mode = mode
            //};

            //var letterAddress = new LetterAddress
            //{
            //    ParagraphIndex = paragraphIndex,
            //    WordIndex = wordIndex,
            //    SymbolIndex = symbolIndex
            //};

            //await _resultService.HintLetter(result, letterAddress);

            Response.StatusCode = (int)HttpStatusCode.OK;
            return Json(wordsInParagraphs[paragraphIndex][wordIndex][symbolIndex]);
        }

        [AllowAnonymous]
        [HttpGet("wordCorrectness/{id}/{paragraphIndex}/{wordIndex}/{value}")]
        public async Task<JsonResult> GetWordCorrectness(string id, int paragraphIndex, int wordIndex, string value)
        {
            value = value.Replace("`", "'");
            var wordsInParagraphs = await _textService.GetWordsInParagraphs(id);
            Response.StatusCode = (int)HttpStatusCode.OK;
            return Json(value.Equals(wordsInParagraphs[paragraphIndex][wordIndex]));
        }

        [AllowAnonymous]
        [HttpPost("wordsForCheck/{id}")]
        public async Task<JsonResult> PostCheckWords(string id, [FromBody]string[] words)
        {
            var formattedWords = words.Select(x => x.Replace("`", "'")).ToArray();
            var wordsInParagraphs = await _textService.GetWordsInParagraphs(id);
            var correctWordLocatorsDtoList = new List<CorrectWordLocatorsDto>();

            foreach (var word in formattedWords)
            {
                var locators = new List<WordLocatorDto>();

                for (int i = 0; i < wordsInParagraphs.Length; i++)
                    for (int j = 0; j < wordsInParagraphs[i].Length; j++)
                        if (word.Equals(wordsInParagraphs[i][j], StringComparison.CurrentCultureIgnoreCase))
                            locators.Add(new WordLocatorDto
                            {
                                ParagraphIndex = i,
                                WordIndex = j,
                                IsCapital = char.IsUpper(wordsInParagraphs[i][j].First())
                            });

                if (locators.Count > 0)
                    correctWordLocatorsDtoList.Add(
                        new CorrectWordLocatorsDto { Locators = locators.ToArray(), Word = word });
            }

            return Json(correctWordLocatorsDtoList);
        }
    }
}
