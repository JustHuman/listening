﻿using AutoMapper;
using Listening.Models.Text;
using Listening.Models.TextViewModels;
using Listening.Repositories;
using Listening.Services;
using Listening.Services.ServiceContracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Listening.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Admin")]
    public class TextController : Controller
    {
        private IRepository<Text, string> _textRepository;
        private readonly ITextService _textService;

        public TextController(IRepository<Text, string> repository, ITextService textService)
        {
            _textRepository = repository;
            _textService = textService;
        }

        // TODO: should be rewritten (add here pagination and filtering and combine this with cache)
        [AllowAnonymous]
        [HttpGet]
        public async Task<IEnumerable<TextDescriptionDto>> GetAllTextsDescription()
        {
            return Mapper.Map<IQueryable<Text>, IEnumerable<TextDescriptionDto>>(
                await _textRepository.GetAll());
        }

        [HttpGet("{textId}")]
        public async Task<TextDto> GetText(string textId)
        {
            //User.Identity.
            //_resultService.BuildResultString();
            return await _textService.GetTextDtoById(textId);
        }

        [HttpPost]
        public string PostText([FromBody]TextDto textDto)
        {
            //Text text = _textService.GenerateTextByDto(textDto);
            //_textRepository.Insert(text);
            _textService.Insert(textDto);
            return $"Success post {textDto.Title}";
        }


        [HttpPut("{id}")]
        public string PutText(string id, [FromBody]TextDto textDto)
        {
            //Text text = _textService.GenerateTextByDto(textDto);

            //_textRepository.Update(text);
            _textService.Update(id, textDto);
            return $"Success put {textDto.Title}";
        }

        [HttpDelete("{id}")]
        public void DeleteText(string id)
        {
            //_textRepository.Delete(id);
            _textService.Delete(id);
        }

        [HttpGet("resaveAllTexts")]
        public void ResaveAllTexts()
        {
            _textService.ResaveAndRecalculateAllTexts();
        }
    }
}
