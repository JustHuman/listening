﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Listening.Models;

namespace Listening.Data.Migrations
{
    public partial class ResultsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "Results",
            //    columns: table => new
            //    {
            //        Id = table.Column<long>(nullable: false),
            //        UserId = table.Column<string>(maxLength: 37, nullable: false),
            //        TextId = table.Column<string>(maxLength: 25, nullable: false),
            //        ResultsEncodedString = table.Column<bool[]>(maxLength: 8000, nullable: false),
            //        Mode = table.Column<char>(nullable: false),
            //        Started = table.Column<DateTime>(nullable: false),
            //        Finished = table.Column<DateTime>(nullable: true),
            //        IsStarted = table.Column<bool>(nullable: false),
            //        IsCompleted = table.Column<bool>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Results", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_Results_AspNetUsers_UserId",
            //            column: x => x.UserId,
            //            principalTable: "AspNetUsers",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.NoAction);
            //    });

            //migrationBuilder.Sql(@"ALTER TABLE ""Results"" 
            //        add constraint ModeConstraint check (""Mode"" like '[js]')");

            //migrationBuilder.Sql($@"
            //    CREATE TABLE '{nameof(Result)}s'
            //    (
            //      '{nameof(Result.Id)}' SERIAL,
            //      '{nameof(Result.UserId)}' character varying(37) NOT NULL,
            //      '{nameof(Result.TextId)}' character varying(25) NOT NULL,
            //      '{nameof(Result.ResultsEncodedString)}' boolean[],
            //      '{nameof(Result.Started)}' character varying(24),
            //      '{nameof(Result.Finished)}' character varying(24),
            //      '{nameof(Result.IsCompleted)}' boolean,
            //      CONSTRAINT 'PK_Results' PRIMARY KEY ('{nameof(Result.Id)}'),
            //      CONSTRAINT 'FK_Results_AspNetUsers_UserId' FOREIGN KEY ('{nameof(Result.UserId)}')
            //          REFERENCES 'AspNetUsers' ('{nameof(ApplicationUser.Id)}') MATCH SIMPLE
            //          ON UPDATE NO ACTION ON DELETE NO ACTION
            //    )
            //    WITH (
            //      OIDS=FALSE
            //    );
            //    ALTER TABLE '{nameof(Result)}s'
            //      OWNER TO postgres;
            //".Replace("'", "\""));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "Results");
        }
    }
}
