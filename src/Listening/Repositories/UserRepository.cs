﻿using Listening.Data;
using Listening.Exceptions;
using Listening.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Listening.Repositories
{
    public class UserRepository : IRepository<ApplicationUser, string>, IUserRepository
    {
        private static ApplicationDbContext _context;
        private static UserManager<ApplicationUser> _userManager;

        public UserRepository(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task Delete(string itemId)
        {
            var user = await _userManager.FindByIdAsync(itemId);
            if (user == null)
                throw new DataException("User is not exist");

            await _userManager.DeleteAsync(user);
            _context.SaveChanges();
        }

        public Task<IQueryable<ApplicationUser>> GetAll()
        {
            throw new NotImplementedException();
        }

        public async Task<ApplicationUser> GetById(string id)
        {
            return await _userManager.FindByIdAsync(id);
        }

        public async Task<ApplicationUser> GetByEmail(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task Insert(ApplicationUser item)
        {
            if (await _userManager.FindByIdAsync(item.Id) != null)
                throw new DataException("User is already exist");
            await _userManager.CreateAsync(item);
            _context.SaveChanges();
        }

        public async Task Update(ApplicationUser item)
        {
            if (await _userManager.FindByIdAsync(item.Id) == null)
                throw new DataException("User is not exist");
            await _userManager.UpdateAsync(item);
            _context.SaveChanges();
        }
    }
}
