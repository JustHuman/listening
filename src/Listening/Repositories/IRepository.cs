﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Listening.Repositories
{
    public interface IRepository<T, Y>
    {
        Task<IQueryable<T>> GetAll();
        Task<T> GetById(Y id);
        Task Insert(T item);
        Task Update(T item);
        Task Delete(Y itemId);
    }
}
