﻿using Listening.Models.Text;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Listening.Repositories
{
    public class TextsMongoRepository : IRepository<Text, string>
    {
        private const string TextId = "TextId";

        private string _collectionName;
        private IMongoClient _client;
        private IMongoDatabase _dataBase;
        private IMongoCollection<Text> _collection;

        public TextsMongoRepository(IConfigurationRoot configuration)
        {
            _client = new MongoClient(configuration["MongoDB:Url"]);
            _dataBase = _client.GetDatabase(configuration["MongoDB:DataBaseName"]);
            _collectionName = configuration["MongoDB:CollectionName"];
            _collection = _dataBase.GetCollection<Text>(_collectionName);
        }

        public async Task Delete(string id)
        {
            var filter = Builders<Text>.Filter.Eq(TextId, ObjectId.Parse(id));
            await _collection.DeleteOneAsync(filter);
        }

        public async Task<IQueryable<Text>> GetAll()
        {
            var text = await _collection.FindAsync(new BsonDocument());
            return text.ToList().AsQueryable();
        }

        public async Task<Text> GetById(string id)
        {
            //var collection = _dataBase.GetCollection<Text>(_collectionName);
            var filter = Builders<Text>.Filter.Eq(TextId, ObjectId.Parse(id));
            var texts = await _collection.FindAsync(filter);
            return texts.ToList().First();
        }

        public async Task Insert(Text item)
        {
            //var collection = _dataBase.GetCollection<Text>(_collectionName);
            await _collection.InsertOneAsync(item);
        }

        public async Task Update(Text item)
        {
            //var collection = _dataBase.GetCollection<Text>(_collectionName);
            var filter = Builders<Text>.Filter.Eq(TextId, item.TextId);
            var update = Builders<Text>.Update
                            .Set(nameof(item.Title), item.Title)
                            .Set(nameof(item.SubTitle), item.SubTitle)
                            .Set(nameof(item.WordsInParagraphs), item.WordsInParagraphs)
                            .Set(nameof(item.AudioName), item.AudioName)
                            .Set(nameof(item.SymbolsCount), item.SymbolsCount);
            await _collection.UpdateOneAsync(filter, update);
        }
    }
}
