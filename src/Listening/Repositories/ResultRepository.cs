﻿using Listening.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using Npgsql;
using System.Data;
using Dapper.Contrib.Extensions;

namespace Listening.Repositories
{
    public class ResultRepository : IResultRepository
    {
        private string _connectionString;

        public ResultRepository(IConfigurationRoot configuration)
        {
            _connectionString = configuration["ConnectionStrings:DefaultConnection"];
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(_connectionString);
            }
        }

        public async Task Delete(long itemId)
        {
            using (var connection = Connection)
            {
                await connection.ExecuteAsync(
                    @"delete from public.""Results"" where ""Id""=@Id", new { Id = itemId });
            }
        }

        public async Task<IQueryable<Result>> GetAll()
        {
            throw new NotImplementedException();
        }

        public async Task<Result> GetNonCompletedResult(Result templateResult)
        {
            using (var connection = Connection)
            {
                var result = await connection.QueryAsync<Result>(
                    @"select * from public.""Results"" where ""UserId""=@UserId
                            and ""TextId""=@TextId and ""Mode""=@Mode and ""Finished"" is null",
                    new { templateResult.UserId, templateResult.TextId, templateResult.Mode });
                return result.FirstOrDefault();
            }
        }

        public async Task<Result> GetById(long id)
        {
            using (var connection = Connection)
            {
                var result = await connection.QueryAsync<Result>(
                    @"select * from public.""Results"" where ""Id""=@Id", new { Id = id });
                return result.FirstOrDefault();
            }
        }

        public async Task Insert(Result item)
        {
            var idParam = string.Empty;
            var idValue = string.Empty;

            if (item.Id > 0)
            {
                idParam = @"""Id"",";
                idValue = @"@Id,";
            }

            var query = $@"INSERT INTO public.""Results"" ({idParam} ""UserId"", ""TextId"",
                             ""ResultsEncodedString"", ""Mode"", ""Started"", ""Finished"", 
                               ""IsStarted"", ""IsCompleted"") 
                        VALUES ({idValue} @UserId, @TextId, @ResultsEncodedString, @Mode,
                                @Started, @Finished, @IsStarted, @IsCompleted)";

            using (var connection = Connection)
            {
                await connection.ExecuteAsync(query, item);
                // TODO: swich to this one using Dapper.Contrib.Extensions 
                // and enhance method to multiple inserting
                //await connection.InsertAsync(item);
            }
        }

        public async Task Update(Result item)
        {
            using (var connection = Connection)
            {
                await connection.ExecuteAsync(
                    @"update public.""Results"" 
                        set ""UserId""=@UserId, ""TextId""=@TextId, 
                            ""ResultsEncodedString""=@ResultsEncodedString,
                            ""Mode""=@Mode,
                            ""IsStarted""=@IsStarted,
                            ""IsCompleted""=@IsCompleted,
                            ""Started""=@Started, 
                            ""Finished""=@Finished
                        where ""Id""=@Id",
                    item);
            }
        }
    }
}
