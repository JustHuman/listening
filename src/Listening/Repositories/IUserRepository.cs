﻿using Listening.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Listening.Repositories
{
    public interface IUserRepository
    {
        Task<ApplicationUser> GetByEmail(string email);
    }
}
