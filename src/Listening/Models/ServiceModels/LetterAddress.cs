﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Listening.Models.ServiceModels
{
    public class LetterAddress
    {
        public int ParagraphIndex { get; set; }
        public int WordIndex { get; set; }
        public int SymbolIndex { get; set; }
    }
}
