﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Listening.Models
{
    public class Result : IIdenticable<long>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [MaxLength(37)]
        public string UserId { get; set; }

        [MaxLength(25)]
        public string TextId { get; set; }

        [MaxLength(8000)]
        public bool[] ResultsEncodedString { get; set; }
        public char Mode { get; set; }
        public DateTime Started { get; set; }
        public DateTime? Finished { get; set; }
        public bool IsStarted { get; set; }
        public bool IsCompleted { get; set; }
    }
}
