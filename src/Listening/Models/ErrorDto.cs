﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Listening.Models
{
    public class ErrorDto
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
