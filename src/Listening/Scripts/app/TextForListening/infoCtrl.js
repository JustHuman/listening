﻿(function () {
    'use strict';

    angular.module('TextForListening')
        .controller('InfoCtrl', function ($scope, $uibModalInstance, $location) {

            console.log($location.path());

            var self = this;

            self.init = function () {
                $scope.hideExceedFields = $location.path().indexOf('/textJoined') > -1;
            };

            $scope.hideExceedFields = false;

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

            self.init();
        });
})();