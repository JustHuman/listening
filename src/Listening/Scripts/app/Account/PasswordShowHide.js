﻿(function () {
    'use strict';

    angular.module('Account')
        .controller('PasswordShowHideCtrl', function ($scope, CheckDataSvc) {

            $scope.passwordError = '';
            $scope.showPasswd = false;

            $scope.changeState = function () {
                $scope.showPasswd = !$scope.showPasswd;
            };

            $scope.$watch('password', function () {
                $scope.passwordCheck();
            });

            $scope.passwordCheck = function () {
                $scope.passwordError = CheckDataSvc.passwordCheck($scope.password);
            };
        })
        .directive('passwordShowHide', function ($templateCache) {
            return {
                restrict: 'E',
                controller: 'PasswordShowHideCtrl',
                scope: {
                    password: '=',
                    shouldValidate: '='
                },
                templateUrl: 'Account/passwordShowHide.html'
            };
        });
})();