﻿(function () {
    'use strict';

    angular.module('Account')
        .controller('LoginCtrl', function ($rootScope, $scope, $state, $location, AccountDataSvc, growl) {
            var self = this;

            self.successLogin = function (response) {
                $scope.userContext.userName = $scope.user.email;
                $scope.userContext.role = response.data.role;
                $rootScope.deniedPath
                    ? $location.path($rootScope.deniedPath)
                    : $state.go('home');
                $rootScope.deniedPath = null;
            };

            self.catchError = function (response) {
                growl.error(response.data.message.replace('\n', '<br />'));
            };

            $scope.errorsFromServer = [];

            $scope.user = {
                email: '',
                password: '',
                rememberMe: false
            };

            $scope.login = function () {
                AccountDataSvc.login($scope.user).then(self.successLogin, self.catchError);
            };
        });
})();